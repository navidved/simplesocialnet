import React from 'react';
import { Menu, Container, Button } from 'semantic-ui-react';

interface IProps {
	openCreateForm: () => void;
}

export const MainNavBar: React.FC<IProps> = ({ openCreateForm }) => {
	return (
		<Menu fixed="top" inverted>
			<Container>
				<Menu.Item header>
					<img src="/assets/images/logo.png" alt="logo" style={{ marginRight: '10px' }} />
					Micro SocialNet
				</Menu.Item>
				<Menu.Item name="messages" />
				<Menu.Item>
					<Button onClick={openCreateForm} positive content="Create Activity" />
				</Menu.Item>
			</Container>
		</Menu>
	);
};
